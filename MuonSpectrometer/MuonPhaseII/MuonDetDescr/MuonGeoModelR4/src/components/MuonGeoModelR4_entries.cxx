/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/
#include "MuonGeoModelR4/MdtReadoutGeomTool.h"
#include "MuonGeoModelR4/MuonDetectorTool.h"
#include "MuonGeoModelR4/MuonGeoUtilityTool.h"

DECLARE_COMPONENT(MuonGMR4::MuonDetectorTool)
DECLARE_COMPONENT(MuonGMR4::MdtReadoutGeomTool)
DECLARE_COMPONENT(MuonGMR4::MuonGeoUtilityTool)
